RPs = {}

local startRP = 0
local currentRP = GameData.Player.Renown.curRenownEarned
local sessionRP = currentRP - startRP
local startMinutesPlayed = GameData.Tome.Statistics.playedTime
local currentMinutesPlayed = GameData.Tome.Statistics.playedTime
local sessionMinutesPlayed = currentMinutesPlayed - startMinutesPlayed
local hoursPlayed = 0
local minutesPlayed = 0
local windowShow = 1

function RPs.Initialize()
	LibSlash.RegisterSlashCmd("rp",function(msg) RPs.Slash(msg) end)
	CreateWindow("RPs", true)
	WindowSetShowing("RPs", true)
	LayoutEditor.RegisterWindow("RPs", L"RPs", L"RPs", false, false, true, nil)
 	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "RPs.Start")
	RegisterEventHandler(SystemData.Events.LOADING_END, "RPs.Start" )
end

function RPs.Start()
	UnregisterEventHandler(SystemData.Events.LOADING_END, "RPs.Start")
	RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_UPDATED, "RPs.Update")
	RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_RANK_UPDATED, "RPs.Reset")
	RPs.Reset()
	RPs.Update()
end

function RPs.SetCurrent()
	currentRP = GameData.Player.Renown.curRenownEarned
	currentMinutesPlayed = GameData.Tome.Statistics.playedTime
end

function RPs.CloseWindow()
   WindowSetShowing("RPs",false)
end

function RPs.Slash(_msg)
	local msg = string.lower(_msg)
	if msg == "" then
		if windowShow == 1 then
			WindowSetShowing("RPs",false)
			windowShow = 0
		else
			WindowSetShowing("RPs",true)
			windowShow = 1
		end
	elseif msg == "reset" then
		RPs.Reset()
	end
end

function RPs.Reset()
	RPs.SetCurrent()
	startRP = currentRP
	sessionRP = 0
	startMinutesPlayed = currentMinutesPlayed
	sessionMinutesPlayed = 0
	RPs.SetWindow()
end

function RPs.Update()
	RPs.SetCurrent()
	sessionMinutesPlayed = currentMinutesPlayed - startMinutesPlayed
	sessionRP = currentRP - startRP
	RPs.SetWindow()
end

function RPs.SetWindow()
	LabelSetText("Rp", towstring(sessionRP .. L" изв."))
	LabelSetText("Time", PlayedTime(sessionMinutesPlayed))
end

function PlayedTime( _currentMinutesPlayed )
	hoursPlayed = math.floor(_currentMinutesPlayed / 60)
	minutesPlayed = _currentMinutesPlayed
	if ( _currentMinutesPlayed >= 60 ) then
		hoursPlayed = math.floor(_currentMinutesPlayed / 60)
		minutesPlayed = _currentMinutesPlayed - (hoursPlayed * 60)
		return(towstring( hoursPlayed .. L"ч " .. minutesPlayed .. L"мин." ))
	elseif ( _currentMinutesPlayed < 60 ) then
		minutesPlayed = _currentMinutesPlayed
		return(towstring( minutesPlayed .. L" мин" ))
	end
end