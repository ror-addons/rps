<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <UiMod name="RPs" version="1.2" date="26/03/2011" >
    <Author name="Xar"/>
    <Description text="Records renown points" />
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.1"/>
    <Dependencies>
      <Dependency name="EA_TomeOfKnowledge" />
      <Dependency name="LibSlash" />
    </Dependencies>
    <Files>
      <File name="rp.lua" />
      <File name="rp.xml" />
    </Files>
    <OnInitialize>
      <CallFunction name="RPs.Initialize" />
    </OnInitialize>
    <WARInfo>
      <Categories>
        <Category name="RVR" />
      </Categories>
    </WARInfo>
  </UiMod>
</ModuleFile>